package com.naufal;

public class userguest extends useradmin {

    public userguest(String idUSer, String username, String nama, String alamat) {
        super(idUSer, username, nama, alamat);
    }

    @Override
    public String getIdUSer() {
        return super.getIdUSer();
    }

    @Override
    public void setIdUSer(String idUSer) {
        super.setIdUSer(idUSer);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getNama() {
        return super.getNama();
    }

    @Override
    public void setNama(String nama) {
        super.setNama(nama);
    }

    @Override
    public String getAlamat() {
        return super.getAlamat();
    }

    @Override
    public void setAlamat(String alamat) {
        super.setAlamat(alamat);
    }
}
