package com.naufal;

import java.util.Scanner;

public class dataflight {

    String kodepesawat;
    String kedatangn;
    String pintu;
    String aktual;
    String status;

    public dataflight(String kodepesawat, String kedatangn, String pintu, String aktual, String status) {
        this.kodepesawat = kodepesawat;
        this.kedatangn = kedatangn;
        this.pintu = pintu;
        this.aktual = aktual;
        this.status = status;
    }

    public String getKodepesawat() {
        return kodepesawat;
    }

    public void setKodepesawat(String kodepesawat) {
        this.kodepesawat = kodepesawat;
    }

    public String getKedatangn() {
        return kedatangn;
    }

    public void setKedatangn(String kedatangn) {
        this.kedatangn = kedatangn;
    }

    public String getPintu() {
        return pintu;
    }

    public void setPintu(String pintu) {
        this.pintu = pintu;
    }

    public String getAktual() {
        return aktual;
    }

    public void setAktual(String aktual) {
        this.aktual = aktual;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
