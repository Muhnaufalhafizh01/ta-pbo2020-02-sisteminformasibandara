package com.naufal;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here

        Scanner input = new Scanner(System.in);

        bandara bandara = new bandara();

        mainmenu:
        while (true){
            System.out.println("|===============================================|");
            System.out.println("|  Selamat Datang Di Sistem Informasi Bandara   |");
            System.out.println("|===============================================|");
            System.out.println("\t1. Admin");
            System.out.println("\t2. Guest");
            System.out.printf("Pilihan : "); int pil1 = input.nextInt();

            switch (pil1){

                case 1 :

                    bandara.login();

                    submenu:
                    while (true){

                        System.out.println("|===============================================|");
                        System.out.println("|                   Menu Admin                  |");
                        System.out.println("|===============================================|");
                        System.out.println("\t1. Tamabah DataFlight");
                        System.out.println("\t2. Melihat Data");
                        System.out.println("\t3. Delay Pesawat");
                        System.out.println("\t4. Admin Record");
                        System.out.println("\t5. Exit");
                        System.out.printf("Pilihan : "); int sub1 = input.nextInt();

                        switch (sub1){

                            case 1 :
                                bandara.inputdataflight();
                                continue submenu;

                            case 2 :
                                bandara.printdataflight();
                                continue submenu;

                            case 3 :
                                bandara.delayubah();
                                continue submenu;

                            case 4 :
                                bandara.listadmin();
                                continue submenu;

                            case 5 :
                                continue mainmenu;

                            default:
                                System.out.println("Tidak Ada !!!!");
                                continue submenu;
                        }
                    }

                case 2 :
                    submenu2:
                    while (true){
                        
                        System.out.println("|===============================================|");
                        System.out.println("|                  Guest Menu                   |");
                        System.out.println("|===============================================|");
                        System.out.println("Selamat Datang Guest ");
                        System.out.println("Jumlah Flight Saat ini " +bandara.getJumlahdatafligt());
                        System.out.println("\t1. Lihat Jadwal Penerbangan");
                        System.out.println("\t2. Exit");
                        System.out.printf("Pilihan : ");
                        int pil2 = input.nextInt();

                        switch (pil2){

                            case 1 :
                                bandara.printdataflight();
                                continue submenu2;

                            case 2 :
                                continue mainmenu;

                            default:
                                System.out.println("Tidak Ada !!!!");
                                continue submenu2;
                        }

                    }

                default:
                    System.out.println("Tidak ada !!!");
                    continue mainmenu;
            }
        }
    }
}
