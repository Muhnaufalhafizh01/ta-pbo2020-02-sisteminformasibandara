package com.naufal;

public class useradmin {

    String idUSer;
    String username;
    String Nama;
    String Alamat;

    public useradmin(String idUSer, String username, String nama, String alamat) {
        this.idUSer = idUSer;
        this.username = username;
        Nama = nama;
        Alamat = alamat;
    }

    public String getIdUSer() {
        return idUSer;
    }

    public void setIdUSer(String idUSer) {
        this.idUSer = idUSer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String nama) {
        Nama = nama;
    }

    public String getAlamat() {
        return Alamat;
    }

    public void setAlamat(String alamat) {
        Alamat = alamat;
    }
}
